﻿using FFXIVCrafting.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes
{
    public abstract class AbstractCraft
    {
        public string Name { get; set; }
        public Condition Condition { get; set; }
        public decimal Quality { get; set; }
        public int Durability { get; set; }
        public decimal Progress { get; set; }
        public List<Ingredient> Ingredients { get; set; }
    }
}
