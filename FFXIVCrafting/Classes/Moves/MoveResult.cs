﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes.Moves
{
    public class MoveResult
    {
        public Craft Craft { get; set; }
        public int CPCost { get; set; }
    }
}
