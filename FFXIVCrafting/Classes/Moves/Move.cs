﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes.Moves
{
    public abstract class Move
    {
        public int QualityChange { get; set; }
        public int ProgressChange { get; set; }
        public int DurabilityChange { get; set; }
        public int ChanceOfSuccess { get; set; }
        public int CPCost { get; set; }
        public int Efficiency { get; set; }

        public abstract MoveResult Execute(Craft c, int rnd, int cp);
    }
}
