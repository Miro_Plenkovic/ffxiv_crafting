﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes.Moves
{
    public class RapidSynthesis : Move
    {
        public RapidSynthesis()
        {
            ProgressChange = 65;
            DurabilityChange = -10;
            QualityChange = 0;
            ChanceOfSuccess = 50;
            CPCost = 0;
            Efficiency = 250;

        }

        public override MoveResult Execute(Craft c, int rnd, int cp)
        {
            if (cp < CPCost) return new MoveResult { Craft = c, CPCost = CPCost };
            if (rnd <= ChanceOfSuccess)
            {
                c.CurrentQuality += QualityChange;
                c.CurrentProgress += ProgressChange;
            }
            c.CurrentDurability += DurabilityChange;
            return new MoveResult { Craft = c, CPCost = CPCost };
        }
    }
}
