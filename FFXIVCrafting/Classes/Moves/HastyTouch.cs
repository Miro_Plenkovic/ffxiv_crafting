﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes.Moves
{
    public class HastyTouch : Move
    {
        public HastyTouch()
        {
            ProgressChange = 0;
            DurabilityChange = -10;
            QualityChange = 25;
            ChanceOfSuccess = 60;
            CPCost = 0;
            Efficiency = 100;
        }

        public override MoveResult Execute(Craft c, int rnd, int cp)
        {
            if (cp < CPCost) return new MoveResult { Craft = c, CPCost = CPCost };
            if (rnd <= ChanceOfSuccess)
            {
                decimal factor;
                switch (c.Condition)
                {
                    case Enums.Condition.Poor:
                        factor = 0.5m;
                        break;
                    case Enums.Condition.Normal:
                        factor = 1m;
                        break;
                    case Enums.Condition.Good:
                        factor = 1.5m;
                        break;
                    default:
                        factor = 2.5m;
                        break;
                }
                c.CurrentQuality += QualityChange * factor;
                c.CurrentProgress += ProgressChange;
            }
            c.CurrentDurability += DurabilityChange;
            return new MoveResult { Craft = c, CPCost = CPCost };
        }
    }
}
