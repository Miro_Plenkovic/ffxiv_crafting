﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes.Moves
{
    public class MastersMend : Move
    {
        public MastersMend()
        {
            ProgressChange = 0;
            DurabilityChange = 30;
            QualityChange = 0;
            ChanceOfSuccess = 100;
            CPCost = 88;
            Efficiency = 100;
        }

        public override MoveResult Execute(Craft c, int rnd, int cp)
        {
            if (cp < CPCost) return new MoveResult { Craft = c, CPCost = CPCost };
            if (rnd <= ChanceOfSuccess)
            {
                c.CurrentQuality += QualityChange;
                c.CurrentProgress += ProgressChange;
            }
            c.CurrentDurability += DurabilityChange;
            return new MoveResult { Craft = c, CPCost = CPCost };
        }
    }
}
