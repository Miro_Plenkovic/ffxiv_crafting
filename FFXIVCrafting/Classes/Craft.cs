﻿using FFXIVCrafting.Classes.Moves;
using FFXIVCrafting.Enums;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FFXIVCrafting.Classes
{
    public class Craft : AbstractCraft
    {
        public Craft(string name, int quality, int durability, int progress, List<Item> ingredients)
        {
            Name = name;
            Condition = Enums.Condition.Normal;
            Quality = quality;
            Durability = durability;
            Progress = progress;
            Ingredients = new List<Ingredient>();
            foreach (Item item in ingredients.DistinctBy(x => x.Name))
            {
                Ingredients.Add(new Ingredient(item.Name, ingredients.FindAll(x => x.Name == item.Name).Sum(x => x.Quantity)));
            }
            CurrentCondition = Enums.Condition.Normal.ToString();
            CurrentQuality = 0;
            CurrentDurability = durability;
            CurrentProgress = 0;
            Buffs = new List<string>();
        }
        public string CurrentCondition { get; set; }
        public decimal CurrentQuality { get; set; }
        public int CurrentDurability { get; set; }
        public decimal CurrentProgress { get; set; }
        public List<string> Buffs { get; set; }
        public Condition Condition { get; set; }
        public int Turn(string action, int cp)
        {
            Type t = Type.GetType($"FFXIVCrafting.Classes.Moves.{action}");
            var x = Activator.CreateInstance(t);
            Random rnd = new Random();
            MoveResult res =((Move)x).Execute(this, rnd.Next(0, 100), cp);
            int conditionRndValue = rnd.Next(0, 100);
            Condition = Condition.Excellent;
            if (conditionRndValue <= 90) Condition = Condition.Good;
            if (conditionRndValue <= 60) Condition = Condition.Normal;
            if (conditionRndValue <= 20) Condition = Condition.Poor;
            Craft c = res.Craft;
            CurrentProgress = (c.CurrentProgress > Progress) ? Progress : c.CurrentProgress;
            CurrentQuality = (c.CurrentQuality > Quality) ? Quality : c.CurrentQuality;
            CurrentDurability = c.CurrentDurability;
            return res.CPCost;
        }
    }
}
