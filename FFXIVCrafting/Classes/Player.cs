﻿using FFXIVCrafting.Classes.Moves;
using FFXIVCrafting.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes
{
    public class Player
    {
        public Player(string username, PlayerClass playerClass, int cp)
        {
            Username = username;
            Class = playerClass.ToString();
            CP = cp;
            Inventory = new List<Item>();
            ActionBar = new List<Move>();
        }
        public string Username { get; set; }
        public string Class { get; set; }
        public int CP { get; set; }
        public List<Item> Inventory { get; set;}
        public List<Move> ActionBar { get; set; }
    }
}
