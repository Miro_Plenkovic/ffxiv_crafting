﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes
{
    public class Item : Ingredient
    {
        public Item(string name, bool quality, int quantity)
        {
            Name = name;
            Quantity = quantity;
            Quality = quality;
        }
        public bool Quality { get; set; }
    }
}
