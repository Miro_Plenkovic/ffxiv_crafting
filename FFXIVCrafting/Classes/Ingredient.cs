﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Classes
{
    public class Ingredient
    {
        public Ingredient(string name, int quantity)
        {
            Name = name;
            Quantity = quantity;
        }
        public Ingredient()
        {
        }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
