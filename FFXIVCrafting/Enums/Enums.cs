﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FFXIVCrafting.Enums
{
    public enum Attribute {
        Durability,
        Progress,
        Quality
    };

    public enum Condition
    {
        Poor,
        Normal,
        Good,
        Excellent
    };
    public enum PlayerClass
    {
        Carpenter,
        Weaver,
        Goldsmith,
        Armorer,
        Blacksmith,
        Culinarian,
        Alchemist,
        Leatherworker
    };
}
