﻿using FFXIVCrafting.Classes;
using FFXIVCrafting.Enums;
using System;
using System.Collections.Generic;

namespace FFXIVCrafting
{
    class Program
    {
        static void Main(string[] args)
        {
            Ingredient ing = new Ingredient("test", 1);
            Item item = new Item("test", true, 1);
            Craft craft = new Craft("craft", 100, 100, 40, new List<Item>() { item });
            Player player = new Player("test", PlayerClass.Alchemist, 200);
            while (craft.Progress > craft.CurrentProgress && craft.CurrentDurability > 0)
            {
                Console.WriteLine("Progress:" + craft.CurrentProgress.ToString() + "/" + craft.Progress.ToString());
                Console.WriteLine("Quality:" + craft.CurrentQuality.ToString() + "/" + craft.Quality.ToString());
                Console.WriteLine("Durability:" + craft.CurrentDurability.ToString());
                Console.WriteLine("CP:" + player.CP.ToString());
                Console.WriteLine("Condition:" + craft.Condition.ToString());
                Console.WriteLine("Choose an action:");
                Console.WriteLine("1. Basic synthesis");
                Console.WriteLine("2. Basic touch");
                Console.WriteLine("3. Rapid synthesis");
                Console.WriteLine("4. Hasty touch");
                Console.WriteLine("5. Master's mend");
                Console.WriteLine("6. Observe");
                Console.WriteLine("7. Standard touch");
                Console.WriteLine("8. Tricks of the trade");
                char x = Console.ReadKey().KeyChar;
                int CPCost = 0;
                switch (x)
                {
                    case '1':
                        CPCost = craft.Turn("BasicSynthesis", player.CP);
                        break;
                    case '2':
                        CPCost = craft.Turn("BasicTouch", player.CP);
                        break;
                    case '3':
                        CPCost = craft.Turn("RapidSynthesis", player.CP);
                        break;
                    case '4':
                        CPCost = craft.Turn("HastyTouch", player.CP);
                        break;
                    case '5':
                        CPCost = craft.Turn("MastersMend", player.CP);
                        break;
                    case '6':
                        CPCost = craft.Turn("Observe", player.CP);
                        break;
                    case '7':
                        CPCost = craft.Turn("StandardTouch", player.CP);
                        break;
                    case '8':
                        CPCost = craft.Turn("TricksOfTheTrade", player.CP);
                        break;
                    default:
                        Console.WriteLine("Error!");
                        break;
                }
                if (player.CP >= CPCost) player.CP -= CPCost;
            }
            if (craft.Progress > craft.CurrentProgress)
            {

                Console.WriteLine("Craft failed!");
            }
            else
            {
                Console.WriteLine("Craft successful!");
            }
        }
    }
}
